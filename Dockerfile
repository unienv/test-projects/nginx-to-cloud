FROM nginx:1.21-alpine

COPY index.html /usr/share/nginx/html/index.html

ENV PORT 80

# Замена порта
CMD sed -i "s/80;/$PORT;/g" /etc/nginx/conf.d/default.conf && \
    nginx -g 'daemon off;'