# Тестовый проект для развертывания в Яндекс.Облако

- `docker build -t cloud-test-project .` - сборка образа
- `docker run -p 80:80 cloud-test-project:latest` - локальный запуск

В `Dockerfile` разработана замена порта для nginx-конфигурации.